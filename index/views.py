from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView
from django.core.cache import cache

import requests
from datetime import timedelta

def get_token():
    token = cache.get('token')
    if not token:
        print('Getting new token')
        res = requests.get('http://test-mixreality.tribexr.com/agora/token2',
                           params={
                                'channel': 'test'
                           })
        token = res.json()
        cache.set('token', token, timedelta(hours=3).total_seconds())
    return token

@method_decorator(never_cache, name='get')
class BoilerPlate(TemplateView):
    def get(self, request, **kwargs):
        context = {
            'agora_app_id': '909bdf6e52b6432db133906fefac4eb0',
        }

        context.update(get_token())
        return self.render_to_response(context)


class DetectTest(BoilerPlate):
    template_name = 'detect.html'


class HelmetTest(BoilerPlate):
    template_name = 'helmet.html'


class AgoraTest(BoilerPlate):
    template_name = 'agora.html'

class ARAgoraTest(BoilerPlate):
    template_name = 'ar_agora.html'




