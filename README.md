To install requirements

``pip install -r requirements.txt``

Run django server

``python manage.py runserver --settings=webar.settings``

The agora + webar demo can then be found at ``127.0.0.1:8000/ar-agora``