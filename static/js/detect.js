/* global Player, Webcam, MediaStreamCapture, Dom, Effect */ // defined in ./BanubaSDK.js
/* global window AgoraRTC appId effectPath token banubaSDK */


// WEBARROCKSFACE.init({
//  canvasId: 'RockCanvas',
//  NNCPath: nnPath, // neural network model
//  callbackReady: function(errCode, spec){
//    if (errCode){
//      console.log('AN ERROR HAPPENS. ERROR CODE =', errCode);
//      return;
//    }
//    [init scene with spec...]
//    console.log('INFO: WEBARROCKSFACE IS READY');
//  }, //end callbackReady()
//
//  // called at each render iteration (drawing loop)
//  callbackTrack: function(detectState){
//    // render your scene here
//    [... do something with detectState]
//  } //end callbackTrack()
// });//end init call
//
/// / Handle errors.
let handleError = function(err) {
    console.log("Error: ", err);
};

let cameraElement;
let cameraCanvas;
let cameraCanvasType;

function getUserVideo() {
    return window.navigator.mediaDevices.getUserMedia({
        video: true,
        audio: false
    });
}

function initClient() {
    console.log("token " + token);
    const client = AgoraRTC.createClient({ mode: "live", role: "host", codec: "h264" });
    client.init(appId, function() {
        console.log("client initialized");
    }, function(err) {
        console.log("client init failed ", err);
    });

    client.join(token, "test", null);

    return client;
}


function drawVideo() {
    console.log("test");
    cameraCanvasType.drawImage(
        cameraElement,
        0,
        0
    );
    cameraCanvasType.restore();
}

function start(){
    WebARRocksFaceDebugHelper.init({
        spec: {
            canvas: document.getElementById("meCanvas"),
            NNCPath: nnStaticPath
        },
        callbackReady: function(err, spec){

        },
        callbackTrack: function(detectState){

        }
    })
}

window.onload = start
//{
//    // WebAR
//    WebARRocksResizer.size_canvas({
//        canvasId: 'meCanvas2',
//        callback: start
//    });

//    // Stream canvas init (offscreen , will be used to multiplex the streams)
//    cameraElement = document.getElementById("meVideo");
////    cameraElement.style = "opacity:0;position:fixed;z-index:-1;left:-100000;top:-100000;";
//
//    cameraCanvas = document.getElementById("meCanvas");
//    cameraCanvas.height = 1080;
//    cameraCanvas.width = 1920;
////    cameraCanvas.style = "opacity:0;position:fixed;z-index:-1;left:-100000;top:-100000;";
//
//    cameraCanvasType = cameraCanvas.getContext("2d");
//
//    var userVideo = getUserVideo();
//    console.log(cameraElement);
//    //    offscreen canvas
//    cameraElement.srcObject = userVideo.value;
//    cameraElement.play();
//
//
//    // Draw frames
//    setInterval(drawVideo, 1000 / 30);
//
//    var realStream = cameraCanvas.captureStream(60);
//    var tracks = realStream.getVideoTracks();
//
////    var client = initClient();
//    var localStream = AgoraRTC.createStream({ audio: false, video: true, streamID: uid});
//    localStream.addTrack(realStream.getVideoTracks()[0]);
//
//    localStream.init();
//    localStream.play("meVideo");
//    //
//    //    // Publish the local stream
//    //    client.publish(localStream, handleError);
//
//


//};

