function initClient() {
    const client = AgoraRTC.createClient({ mode: "rtc", codec: "h264" });
    client.init(appId, function() {
        console.log("client initialized");
    }, function(err) {
        console.log("client init failed ", err);
    });

    client.join(token, "test", uid);

    return client;
}
//
function startAgora() {
  client = initClient();

  var agoraStream = AgoraRTC.createStream({streamID: uid, audio: false, video: true, uid: uid});
  agoraStream.init(function () {
    agoraStream.play("local_stream", {fit: "contain"});
    client.publish(agoraStream);
  });
  console.log('TEST');
}

let client;

window.addEventListener('load', startAgora);